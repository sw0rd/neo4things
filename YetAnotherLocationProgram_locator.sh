#!/bin/bash
#Auxiliary Script for YeaAnotherLocationProgram.py,
#that converts a given URL into an Ip-Addres, then return its geographic location.
ADDRESS=$(host $1 | awk '/has address/ { print $4 }' | head --lines=1 ) #Changeble if it necessary

curl ipinfo.io/$ADDRESS/loc # /loc return just the coordinate
