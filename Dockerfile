FROM docker.io/library/python:3.8-buster

WORKDIR /app/neogit
RUN \
    apt-get update && \
    apt-get upgrade -yqq && \
    apt-get install -yqq git virtualenv && \
    python3 -m venv env && \
    /bin/bash -c "source env/bin/activate"
COPY . .
RUN pip3 install -r requirements.txt
WORKDIR /app
RUN git clone https://github.com/neo4j-contrib/django-neomodel
WORKDIR /app/django-neomodel
RUN python3 setup.py install
WORKDIR /app/neogit
RUN rm -rf neo4things/files && \
    git clone https://gitlab.freedesktop.org/sw0rd/MUD-Files neo4things/files.git

EXPOSE 8000/tcp
ENTRYPOINT /app/neogit/docker-entrypoint.sh
