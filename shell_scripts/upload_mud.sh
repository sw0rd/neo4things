#!/bin/sh

mudfile="$1"
SERVER="$MUDAPI_SERVER:$MUDAPI_PORT"
USERINFO="$MUDAPI_USER:$MUDAPI_PASS"

python3 upload_mud_request.py "$mudfile" \
    | http -a "$USERINFO" POST http://$SERVER/mud/upload

sleep 1
exit 0
