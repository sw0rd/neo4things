import json
import pprint
import random

import mudapi
from mudapi.models import *
from muddy.maker import make_mud, make_acl_names, make_policy, make_acls, make_support_info
from muddy.models import Direction, IPVersion, Protocol, MatchType

# import the logging library
import logging

# Get an instance of a logger
from mudapi.models.os import OS
from mudapi.models.manager import Manager
from mudapi.models.manufacturer import Manufacturer
from mudapi.models.queries import connections_of_mud
from mudapi.models.thing import Thing
from mudapi.models.relationships import *
from mudapi.models.service import Service
from mudapi.models.mud import MUD
from mudapi.models.router import Router
from mudapi.services import serialize_mud

logger = logging.getLogger(__file__)

# tcom = Manufacturer(name="Telekom")
bms = Manufacturer(
    name="BMS"
)  # MUD URL does not define name of Manufacturer: Use domain?
atheros = Manufacturer(name="Atheros")
ietf = Manufacturer(name="ietf")
logger.info("saving manufacturer: " + bms.name + ", ")
# tcom.save()
bms.save()
atheros.save()
ietf.save()

foo_service = Service(name="Foo Service")  # defualt = thing.name + service
logger.info("saving Service: " + foo_service.name)
foo_service.save()
bms.developer.connect(foo_service)

mud = MUD(
    mud_version=1,
    mud_url="https://lighting.example.com/lightbulb2000",
    mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
    systeminfo="The BMS Example Light Bulb",
    name="Bar MUD",  # this can be found form the mud url after the domain
    firmware_rev="0.1",  # Liam
    software_rev="0.1",  # Liam
    documentation="https://lighting.example.com/documentation",  # von Liam
)
logger.info("saving MUD: " + mud.name)
mud.save()
mud.operator.connect(bms)  # Create edge between bms and the thing

local_manager = Manager(
    ip_internal="192.168.1.1", ip_external="1.0.4.5", name="MUD Manager"
)
local_manager.save()

thing = Thing(
    mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan"
)
logger.info("saving Network Card: " + thing.mac_addr)
thing.save()
thing.mac_vendor.connect(atheros)
thing.device_vendor.connect(ietf)
thing.manager.connect(local_manager)
thing.mud.connect(mud)  # This line conencts the network Card with the MUD File

# router = Router().save()
# thing.router.connect(router)

linux = OS(family="Debian", gen="3.13 - 3.16", name="Debian 10")
linux.save()
linux.network.connect(thing)

logger.info("creating connections")

# from device policy -> acl -> aces -> acl
m2s = mud.acl.connect(
    foo_service,
    {
        "name" : "cl0-todev",
        "type": "6t",
        "acl_dns": "test.example.com",  # matches
        "port": [443],  # tcp
        "direction_initiated": "from-device",  # tcp
        "forwarding": "accept",  # actions
    },
)

s2m = thing.acl.connect(
    foo_service,
    {

        "type": "6t",
        "acl_dns": "test.example.com",  # matches
        "port": [443],  # tcp
        "direction_initiated": "from-device",  # tcp
    },
)


# to device policy -> acl -> aces -> acl
foo_service.mud.connect(
    mud,
    {
        "name": "cl0-frdev",
        "type": "6t",
        "acl_dns": "test.example.com",  # matches
        "port": [443],  # tcp
        "direction_initiated": "from-device",  # tcp
        "forwarding": "accept",  # actions
    },
)


# This is always added because this is a default value (RFC 8252) TODO Chapter
dns_service = Service(name="DNS Service", port=[53])
dns_service.save()
ntp_service = Service(name="NTP Service", port=[123])
ntp_service.save()

# Initiliase the default connections
mud.acl.connect(ntp_service, {"type": "6u", "name": "ent0-frdev", "port": [123]})
ntp_service.mud.connect(mud, {"type": "6u", "name": "ent1-frdev", "port": [53]})

mud.acl.connect(ntp_service, {"type": "6u", "name": "ent0-todev", "port": [123]})
dns_service.mud.connect(mud, {"type": "6u", "name": "ent1-todev", "port": [53]})

json_mud = serialize_mud(mud)
# pprint.pprint(json_mud)
# print(json_mud)