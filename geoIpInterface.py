# The Databases "GeoLite2-ASN.mmdb" und "GeoLite2-City.mmdb" should be on the same folder as this file. It is recommended to update these regurlarly
import geoip2.database
import os
from sys import argv
import ipaddress

if len(argv) != 2:
    print("Too many arguments")

try:
    ipaddress.ip_address(argv[1])
except:
    print("Not a valid IP_Address")

dbPath = os.getcwd()
with geoip2.database.Reader(dbPath + '/GeoLite2-ASN.mmdb') as reader:
    response = reader.asn(argv[1])
    print("DNS-ASN:", response.autonomous_system_organization)  # Prints the ASN

with geoip2.database.Reader(dbPath + '/GeoLite2-City.mmdb') as reader:
    response = reader.city(argv[1])
    if response.country.iso_code is not None:
        print("Country:", response.country.iso_code)
    else:
        print("Not Found")

    if response.city.name is not None:
        print("City:", response.city.name)
    else:
        print("Not Found")

    if response.location.latitude is not None:
        print("Approximate Geolocation:", '[', response.location.latitude, ',', response.location.longitude, ']')
    else:
        print("Not Found")
