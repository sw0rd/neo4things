from django_cron import CronJobBase, Schedule

from mudapi.services import import_muds


class UpdateMUDCronJob(CronJobBase):
    RUN_EVERY_MINS = 120 # every 2 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'mudapi.Update_cron'    # a unique code

    def do(self):
        import_muds()