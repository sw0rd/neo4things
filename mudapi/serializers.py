#!/usr/bin/env python3
from datetime import datetime

import pytz
import re
from django.contrib.auth.models import User, Group
from rest_framework import serializers

from mudapi.models.manager import Manager
from mudapi.models.manufacturer import Manufacturer
from mudapi.models.mud import MUD
from mudapi.models.os import OS
from mudapi.models.relationships import ACLRelationship
from mudapi.models.service import Service
from mudapi.models.thing import Thing


class TimestampField(serializers.ReadOnlyField):
    def to_representation(self, value):
        return value


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["url", "username", "email", "groups"]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "name"]


class MUDSerializer(serializers.Serializer):
    mud_version = serializers.IntegerField()
    mud_url = serializers.URLField()
    mud_signature = serializers.CharField(max_length=128)
    cache_validity = serializers.IntegerField(max_value=168)
    systeminfo = serializers.CharField(max_length=512)
    name = serializers.CharField(max_length=64)
    firmware_rev = serializers.CharField(max_length=5)
    software_rev = serializers.CharField(max_length=5)
    documentation = serializers.URLField()

    def create(self, **validated_data):
        """
        Create and return a new `MUD` instance, given the validated data.
        """
        m = MUD(**validated_data)
        m.save()
        return m


class ServiceSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=50)
    product = serializers.CharField(max_length=50)
    method = serializers.CharField(max_length=50)

    def create(self, validated_data):
        """
        Create and return a new `Service` instance, given the validated data.
        """
        s = Service(**validated_data)
        s.save()
        return s


class ManagerSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=50)
    ip_internal = serializers.IPAddressField()
    ip_external = serializers.IPAddressField()

    def create(self, validated_data):
        """
        Create and return a new "Manager" instace, given the validated data.
        """
        m = Manager(**validated_data)
        m.save()
        return m


class ManufacturerSerializer(serializers.Serializer):
    uid = serializers.UUIDField()
    name = serializers.CharField(max_length=50)

    def create(self, **validated_data):
        """
        Create and return a new "Manufacturer" instace, given the validated data.
        """
        m = Manufacturer(**validated_data)
        m.save()
        return m


class ThingSerializer(serializers.Serializer):
    serial = serializers.CharField(max_length=64)
    mac_addr = serializers.CharField(min_length=10, max_length=32) # DUID is fallback
    ipv4_addr = serializers.IPAddressField()
    ipv6_addr = serializers.IPAddressField()
    hostname = serializers.CharField(max_length=512)

    def create(self, validated_data):
        """
        Create and return a new "Thing" instace, given the validated data.
        """
        t = Thing(**validated_data)
        t.save()
        return t

    def validate(self, t):
        if not re.match(r"([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}", t["mac_addr"]):
            if not re.match(r"^(?:[A-Fa-f0-9]{2}):(?:[A-Fa-f0-9]{2})(?::(?:[A-Fa-f0-9]{2})){1,128}$", t["mac_addr"]):
                raise serializers.ValidationError("invalid MAC/DUID");
        return t




class OSSerializer(serializers.Serializer):
    family = serializers.CharField(max_length=50)
    gen = serializers.CharField(max_length=50)
    name = serializers.CharField(max_length=50)

    def create(self, **validated_data):
        """
        Create and return a new "Operating System" instace, given the validated data.
        """
        o = OS(self, **validated_data)
        o.save()
        return o


class UploadMudSerializer(serializers.Serializer):
    serial = serializers.CharField(max_length=4000)


class MudByManufacturerSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=50)


class ACLSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=40,allow_blank=True)
    type = serializers.CharField(max_length=40)
    acl_dns = serializers.CharField(max_length=512)
    port = serializers.ListField(child=serializers.IntegerField())
    direction_initiated = serializers.CharField(max_length=40)
    forwarding = serializers.CharField(max_length=50)
    timestamp = TimestampField(initial=datetime.now(pytz.utc), allow_null=True)


    def create(self, **validated_data):
        """
        Create and return a new ACL
        """
        a = ACLRelationship(self, **validated_data)
        a.save()
        return a

    def validate(self, t):
        if not t['type'] in ["4t", "4u", "4a", "6t", "6u", "6a"]:
            raise serializers.ValidationError("invalid type");
        return t

    class Meta:
        model = User
        fields = ['name', 'type', 'acl_dns' 'port', 'direction_initiated', 'forwarding', 'timestamp']



class DescriptionSerializer(serializers.Serializer):
    # TODO validate for semantic correctness
    mud_url = serializers.CharField(max_length=512)
    mac_addr = serializers.CharField(min_length=10, max_length=17)



class ForkSerializer(serializers.Serializer):
    # TODO validate for semantic correctness
    mud_url = serializers.CharField(max_length=512)
    name = serializers.CharField(max_length=64)

class IPSerializer(serializers.Serializer):
    ip = serializers.IPAddressField()


class MUDJsonSerializer(serializers.Serializer):
    mud_url = serializers.URLField
    json = serializers.CharField(max_length=512*1024)


class GeneratorSerializer(serializers.Serializer):
    file = serializers.FileField()
#    mud_url = serializers.URLField()
    gateway_mac = serializers.CharField(max_length=17)
    gateway_ipv4 = serializers.IPAddressField()
    gateway_ipv6 = serializers.IPAddressField()
    device_name = serializers.CharField(max_length=64)
    device_mac = serializers.CharField(max_length=17)

    class Meta:
        fields = ['file_uploaded', 'gateway_mac', 'gateway_ipv4', 'gateway_ipv6', 'device_name', 'device_mac']
