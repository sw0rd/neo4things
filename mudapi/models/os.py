#!/usr/bin/env python3

from neomodel import StructuredNode, StringProperty, RelationshipTo

from mudapi.models.thing import Thing


class OS(StructuredNode):
    """
    This is an Operating System
    """

    family = StringProperty()
    gen = StringProperty()
    name = StringProperty(unique_index=True)

    # Relations
    network = RelationshipTo(Thing, "RUNS")
