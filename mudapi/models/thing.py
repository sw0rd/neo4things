#!/usr/bin/env python3
from django_neomodel import DjangoNode
from neomodel import (
    StringProperty,
    RelationshipTo,
)

from mudapi.models.relationships import ACLRelationship


class Thing(DjangoNode):
    """
    This is a Thing

    Any thing uses a Network Card to interface with the Internet

    A network card can have a realtioship to a thing, where it is
    descriped by this mud_url, also a vendor operates a MUD

    We use the mac adress as an unique index, all the other properties are optional:
    - mac
    - ipv4
    - ipv6
    - hostname
    - serial
    - vendor

    """

    serial = StringProperty(unique_index=True)
    mac_addr = StringProperty(
        unique_index=True, required=True
    )  # There is only one device with this adress
    ipv4_addr = StringProperty()
    ipv6_addr = StringProperty()
    hostname = StringProperty()

    # Relations
    # Device Related
    device_vendor = RelationshipTo(".manufacturer.Manufacturer", "OPERATED_BY")
    mud = RelationshipTo(".mud.MUD", "DESCRIBED_BY")
    router = RelationshipTo(".router.Router", "ROUTED_BY")
    mac_vendor = RelationshipTo(".manufacturer.Manufacturer", "HAS_MAC_OF")
    manager = RelationshipTo(".manager.Manager", "MANAGED_BY")
    acl = RelationshipTo(".service.Service", "ACL", model=ACLRelationship)

    def services(self, name):
        """Get all ACL Connections which are allowed"""
        results, columns = self.cypher(
            """MATCH (a:Thing)-[:DESCRIBED_BY]->(:MUD)<-[:ACL]-(s:Service {name: $name})
               RETURN a"""
        )
        return [self.inflate(row[0]) for row in results]
