#!/usr/bin/env python3
from django_neomodel import DjangoNode
from neomodel import (
    StringProperty,
    IntegerProperty,
    UniqueIdProperty,
    RelationshipTo,
)

from mudapi.models.manufacturer import Manufacturer
from mudapi.models.relationships import MudUpdateRelationship, ACLRelationship


class MUD(DjangoNode):
    """
    IOT Deivce as specified by RFC 8520, The thing is the UID
    The rest is mud
    TODO make distinction?

    List Of Properties:

    mud-url {
         type inet:uri;
         mandatory true;
         description
           "This is the MUD URL associated with the entry found
            in a MUD file.";
       }
       last-update {
         type yang:date-and-time;
         mandatory true;
         description
           "This is intended to be when the current MUD file
            was generated.  MUD managers SHOULD NOT check
            for updates between this time plus cache validity.";
       }
       mud-signature {
         type inet:uri;
         description
           "A URI that resolves to a signature as
            described in this specification.";
       }
       cache-validity {
         type uint8 {
           range "1..168";
         }
         units "hours";
         default "48";
         description
           "The information retrieved from the MUD server is
            valid for these many hours, after which it should

      is-supported {
         type boolean;
         mandatory true;
         description
           "This boolean indicates whether or not the MUD is
            currently supported by the manufacturer.";
       }
       systeminfo {
         type string;
         description
           "A UTF-8 description of this MUD.  This
            should be a brief description that may be
            displayed to the user to determine whether
            to allow the MUD on the
            network.";

       model-name {
         type string;
         description
           "Model name, as described in the
            ietf-hardware YANG module.";

       firmware-rev {
         type string;
         description
           "firmware-rev, as described in the
            ietf-hardware YANG module.  Note that this field
            MUST NOT be included when the device can be
            updated but the MUD URL cannot.";

      software-rev:
         type string;
         description
           "software-rev, as described in the
            ietf-hardware YANG module.  Note that this field
            MUST NOT be included when the device can be
            updated but the MUD URL cannot.";


      documentation:
         type inet:uri;
         description
           "This URL points to documentation that
            relates to this device and any classes that it uses
            in its MUD file.  A caution: MUD managers need
            not resolve this URL on their own but rather simply
            provide it to the administrator.  Parsing HTML is
            not an intended function of a MUD manager.";


    List of Relationships:

    ipv6_to: IPV6
    ipv6_from: IPV6
    lan_to: LAN_TO
    lan_from: LAN_FROM
    operator: OPERATED_BY

    """

    uid = UniqueIdProperty()
    mud_version = IntegerProperty(default=1)
    mud_url = StringProperty(unique_index=True, required=True)

    mud_signature = StringProperty(required=True)
    cache_validity = IntegerProperty(
        index=True, default=48
    )  # Default is 48 Hours, max is 168
    # is_supported : Boolean TODO
    systeminfo = StringProperty(required=True)
    name = StringProperty()
    firmware_rev = StringProperty(required=True)
    software_rev = StringProperty(required=True)
    documentation = StringProperty(required=True)

    # Relations:
    send_lan = RelationshipTo("MUD", "LAN_TO")
    send_from = RelationshipTo("MUD", "LAN_FROM")
    last_update = RelationshipTo(
        Manufacturer, "UPDATED_BY", model=MudUpdateRelationship
    )
    operator = RelationshipTo(Manufacturer, "MANUFACTURED_BY")
    acl = RelationshipTo(".service.Service", "ACL", model=ACLRelationship)

    class Meta:
        app_label = "mud"
