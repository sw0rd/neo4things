# for standalone queries
from neomodel import db

from mudapi.models import MUD, ACLRelationship, Service, Thing


def mud_from_connections(acl_dns):
    """Find all MUD Files which contains all the connections TODO $connections or [$connections]"""
    results, columns = db.cypher_query(
        """
        WITH $acl_dns as acl_dns
        MATCH (m:MUD)-[r:ACL]->(:Service)
        WHERE r.acl_dns in acl_dns
        WITH m, size(acl_dns) as inputCnt, count(DISTINCT r) as cnt
        WHERE cnt = inputCnt
        RETURN m
        """,
        {"acl_dns": acl_dns},
    )
    return [MUD.inflate(row[0]) for row in results]


def mud_from_thing(mac_addr):
    """Find all MUD Files which contains all the connections TODO $connections or [$connections]"""
    results, columns = db.cypher_query(
        """
        WITH $mac_addr as mac_addr
        MATCH (a:Thing {mac_addr:$mac_addr })-[c]->(:Service)
        MATCH (m:MUD)-[r:ACL]->(:Service)
        WHERE r.acl_dns in c.acl_dns
        WITH m, count(DISTINCT c) as inputCnt, count(DISTINCT r) as cnt
        WHERE inputCnt <= cnt
        RETURN m
        """,
        {"mac_addr": mac_addr},
    )
    return [MUD.inflate(row[0]) for row in results]


def allowed_connections_of_thing(mac_addr):
    """Get all ACL Connections which are allowed"""
    results, columns = db.cypher_query(
        """
        MATCH (a:Thing {mac_addr: $mac_addr})-[:DESCRIBED_BY]->(:MUD)-[c:ACL]->(:Service)
        RETURN DISTINCT c
        """,
        {"mac_addr": mac_addr},
    )
    return [ACLRelationship.inflate(row[0]) for row in results]


def connections_of_thing(mac_addr):
    """Get all Connections of  a Thing which are allowed"""
    results, columns = db.cypher_query(
        """
        MATCH (a:Thing {mac_addr: $mac_addr})-[c:ACL]->(:Service)
        RETURN c
        """,
        {"mac_addr": mac_addr},
    )
    return [ACLRelationship.inflate(row[0]) for row in results]


def connections_of_mud(mud_url):
    """Get all Connections of  a MUD which are allowed"""
    results, columns = db.cypher_query(
        """
        MATCH (a:MUD {mud_url: $mud_url})-[c:ACL]-(:Service)
        RETURN c
        """,
        {"mud_url": mud_url},
    )
    return [ACLRelationship.inflate(row[0]) for row in results]

def mud_of_thing(mud_url):
    """Get all Things which are described by a MUD URL"""
    results, columns = db.cypher_query(
        "MATCH (t:Thing)-[r:DESCRIBED_BY]->(:MUD {mud_url: $mud_url}) RETURN t",
        {"mud_url", mud_url},
    )
    return [Thing.inflate(row[0]) for row in results]


def services_of_thing(mac_addr):
    """Get all ACL Connections which are allowed"""
    results, columns = db.cypher_query(
        """MATCH (a:Thing {mac_addr: $mac_addr})-[:DESCRIBED_BY]->(:MUD)-[:ACL]->(s:Service)
           RETURN s""",
        {"mac_addr": mac_addr},
    )
    return [Service.inflate(row[0]) for row in results]


def mud_from_port(ports):
    """Get all MUD which allow connection to all specified ports in range"""
    results, columns = db.cypher_query(
        """ MATCH (m:MUD)-[r:ACL {port: $ports} ]->(:Service) RETURN DISTINCT m""",
        {"ports": ports},
    )
    return [MUD.inflate(row[0]) for row in results]


def mud_from_schema(schema):
    """Get all MUD which allow connection to a specific network Protocol schema (ssh, ipp, etc)"""
    results, columns = db.cypher_query(
        """MATCH (:Service)<-[:PROTOCOL_TO {schema: $schema}]-(m:MUD) RETURN DISTINCT m""",
        {"schema": schema},
    )
    return [MUD.inflate(row[0]) for row in results]


def mud_allowed_dns(dns_name):
    """Get all MUD which allow connection to a domain-name server"""
    results, columns = db.cypher_query(
        """MATCH p= (:Service) <- [:ACL_TO {dns-name: $dns_name}]-(m:MUD) RETURN DISTINCT m""",
        {"dns-name": dns_name},
    )
    return [MUD.inflate(row[0]) for row in results]


def thing_from_mac_vendor(mac_vendor):
    """Returns all Things which have this specfied mac vendor"""
    results, columns = db.cypher_query(
        """MATCH p= (:Manufacturer {name: $mac_vendor} ) <-[:HAS_MAC_OF]-(:Thing)-[r:DESCRIBED_BY]->(m:MUD) RETURN m""",
        {"mac_vendor": mac_vendor},
    )
    return [Thing.inflate(row[0]) for row in results]


def things_of_a_manager(ip_external):
    """Return all Things which are managed by a manager"""
    results, columns = db.cypher_query(
        """MATCH p= (:Manager {ip_external: $ip_external})<-[:MANAGED_BY]-(t:Thing)-[r:DESCRIBED_BY]->(:MUD) RETURN T""",
        {"ip_external": ip_external},
    )
    return [Thing.inflate(row[0]) for row in results]
