#!/usr/bin/env python3
from neomodel import StructuredNode, UniqueIdProperty, StringProperty


class Manager(StructuredNode):
    """
    A manufactor operates things and service

    Every Manager is unique, we can filter requests to different networks
    by filtering Nodes by the predicate if they are connect to a specific Manager

    A manager can be named and have a unique ip
    """

    uid = UniqueIdProperty()
    name = StringProperty(default="Manager")
    ip_internal = StringProperty(unique_index=True)
    ip_external = StringProperty(unique_index=True)

    # Relations
