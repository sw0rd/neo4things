#!/usr/bin/env python3
from neomodel import StructuredNode, UniqueIdProperty, StringProperty, RelationshipFrom


class Manufacturer(StructuredNode):
    """
    A manufactor operates things and service

    They provice MUD Files
    """

    uid = UniqueIdProperty()
    name = StringProperty(unique_index=True)

    # Relations

    developer = RelationshipFrom(".service.Service", "MANUFACTURED_BY")
