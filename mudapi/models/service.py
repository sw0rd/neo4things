#!/usr/bin/env python3
from neomodel import StructuredNode, UniqueIdProperty, StringProperty, RelationshipTo

from mudapi.models import ACLRelationship


class Service(StructuredNode):
    """
    A generic Service on which software is run

    Can connect to a thing via dns
    Can connect to a thing via a specified protocol
    """

    uid = UniqueIdProperty()
    name = StringProperty(
        unique_index=True, required=True
    )  # if not known, take thing name and append Serivce
    product = StringProperty()
    method = StringProperty()

    # Relations:
    thing = RelationshipTo(".thing.Thing", "ACL", model=ACLRelationship)
    mud = RelationshipTo(".mud.MUD", "ACL", model=ACLRelationship)
