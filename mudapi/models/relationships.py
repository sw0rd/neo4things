#!/usr/bin/env python3
from datetime import datetime
import pytz
from neomodel import (
    StringProperty,
    IntegerProperty,
    StructuredRel,
    DateProperty,
    ArrayProperty,
    DateTimeProperty
)


class MudUpdateRelationship(StructuredRel):
    """
    A very simple relationship between the manufacturer and a IOT device
    records the date at which an MUD file was updated.
    """

    on_date = DateProperty(default_now=True)


class ACLRelationship(StructuredRel):
    """
    The choice of these particular points in the access control list
    model is based on the assumption that we are in some way referring to
    IP-related resources, as that is what the DNS returns.  A domain name
    in our context is defined in [RFC6991].

    The argument corresponds to the domain name as specified by
    inet:host.

    default value is 443 for https
    """

    name = StringProperty(default="Unamed Connection")
    PROTOCOLS = {"4u": "IPV4 UDP", "4t": "IPV4 TCP", "6t": "IPV6 TCP", "6u": "IPV6 UDP", "6a": "IPV6 ANY", "4a": "IPV4 ANY"}
    type = StringProperty(required=True, choices=PROTOCOLS)
    acl_dns = StringProperty()
    port = ArrayProperty(IntegerProperty())
    direction_initiated = StringProperty()
    timestamp = DateTimeProperty(
        default=lambda: datetime.now(pytz.utc)
    )

    # Actions
    forwarding = StringProperty()
