#!/usr/bin/env python3

import json
# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__file__)


from django.contrib.auth.models import User, Group
from django.core.files.storage import default_storage
from django.http import JsonResponse
from drf_spectacular.utils import extend_schema, OpenApiParameter, extend_schema_view
from mac_vendor_lookup import MacLookup
from neomodel import db
from rest_framework import generics, parsers
from rest_framework import permissions
from rest_framework import viewsets, status
from rest_framework.decorators import parser_classes
from rest_framework.exceptions import NotFound
from rest_framework.parsers import JSONParser, FileUploadParser
from rest_framework.response import Response
from rest_framework.validators import ValidationError
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet

from mudapi.models.manager import Manager
from mudapi.models.manufacturer import Manufacturer
from mudapi.models.mud import MUD
from mudapi.models.os import OS
from mudapi.models.queries import (
    mud_from_connections,
    allowed_connections_of_thing,
    services_of_thing,
    mud_from_thing,
    mud_from_port,
    things_of_a_manager,
    mud_from_schema,
    thing_from_mac_vendor,
    connections_of_thing,
    mud_of_thing, connections_of_mud,
)
from mudapi.models.service import Service
from mudapi.models.thing import Thing
from mudapi.mud.parser import MudParser
from mudapi.serializers import (
    UserSerializer,
    GroupSerializer,
    ServiceSerializer,
    ThingSerializer,
    ManagerSerializer,
    ManufacturerSerializer,
    OSSerializer,
    UploadMudSerializer,
    MudByManufacturerSerializer,
    MUDSerializer,
    ACLSerializer,
    DescriptionSerializer,
    IPSerializer, MUDJsonSerializer, GeneratorSerializer, ForkSerializer,
)

# which class sohuld be used https://www.django-rest-framework.org/api-guide/generic-views/#listapiview
from mudapi.services import serialize_mud, get_service_name, generate_mud, save_mud_gitlab


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class MUDViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Things to be edited or viewed.
    """

    serializer_class = MUDSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    lookup_field = "mud_url"

    def get_queryset(self):
        return MUD.nodes.all()

    def retrieve(self, request, *args, **kwargs):
        mud = Manufacturer.nodes.get_or_none(name=kwargs["mud_url"])
        if mud is None:
            raise NotFound(detail="Error 404, page not found", code=404)
        serializer = ManufacturerSerializer(mud)
        return Response(serializer.data)


class ServiceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Services to be edited or viewed.
    """

    serializer_class = ServiceSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    lookup_field = "name"

    def get_queryset(self):
        return Service.nodes.all()

    def retrieve(self, request, *args, **kwargs):
        service = Service.nodes.get_or_none(name=kwargs["name"])
        if service is None:
            raise NotFound(detail="Error 404, page not found", code=404)
        serializer = ServiceSerializer(service)
        return Response(serializer.data)


class ManagerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Managers to be edited or viewed.
    """

    serializer_class = ManagerSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = "name"

    def get_queryset(self):
        return Manager.nodes.all()

    def retrieve(self, request, *args, **kwargs):
        manager = Manager.nodes.get_or_none(name=kwargs["name"])
        if manager is None:
            raise NotFound(detail="Error 404, page not found", code=404)
        serializer = ManagerSerializer(manager)
        return Response(serializer.data)


class ManufacturerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Manufacturers to be edited or viewed.
    """

    serializer_class = ManufacturerSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    lookup_field = "name"

    def get_queryset(self):
        return Manufacturer.nodes.all()

    def retrieve(self, request, *args, **kwargs):
        manufacturer = Manufacturer.nodes.get_or_none(name=kwargs["name"])
        if manufacturer is None:
            raise NotFound(detail="Error 404, page not found", code=404)
        serializer = ManufacturerSerializer(manufacturer)
        return Response(serializer.data)


class ThingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Things to be edited or viewed.
    """

    serializer_class = ThingSerializer
    permission_classes = [permissions.IsAuthenticated] # TODO filter by user
    lookup_field = "mac_addr"

    def get_queryset(self):
        return Thing.nodes.all()

    def retrieve(self, request, *args, **kwargs):
        thing = Thing.nodes.get_or_none(mac_addr=kwargs["mac_addr"])
        if thing is None:
            raise NotFound(detail="Error 404, page not found", code=404)
        serializer = ThingSerializer(thing)
        return Response(serializer.data)


class OSViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows OSs to be edited or viewed.
    """

    queryset = OS.nodes.all()
    serializer_class = OSSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    lookup_field = "name"

    def retrieve(self, request, *args, **kwargs):
        os = OS.nodes.get(name=kwargs["name"])
        serializer = OSSerializer(os)
        return Response(serializer.data)


class MudByManufacturerViewSet(generics.ListCreateAPIView):
    """
    API endpoint that allows admins to upload MUD-files
    """

    # TODO move query to queries.py
    serializer_class = MudByManufacturerSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Manufacturer.nodes.all()

    @parser_classes((JSONParser,))
    def create(self, request):
        json_data = request.data
        manufacturer = json_data["name"]
        try:
            res = db.cypher_query(
                """
                        MATCH  (p:Manufacturer {name: $manufacturer} )
                    <-[:HAS_MAC_OF]-(:Thing)-[r:DESCRIBED_BY]->
                        (m:MUD) RETURN m.mud_url
                        """,
                {"manufacturer": manufacturer},
            )[0]
            result = []
            for i in res:
                result.append(i[0])
            response = {"result": result}
            return JsonResponse(response, safe=False)
        except:
            response = {"error": "Error occurred"}
            return JsonResponse(response, safe=False)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="acl_dns",
                description="DNS Names which have been accessed",
                required=True,
                type=str,
            )
        ]
    )
)
class MUDByDNSView(generics.ListAPIView):
    """Return a set of MUD files which match all the connections specified in acl_dns
    MUD files are only returned if all of the connections are allowed"""

    serializer_class = MUDSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Manufacturer.nodes.all()

    def get_queryset(self):
        acl_dns = self.request.query_params.get("acl_dns")
        return mud_from_connections(acl_dns=acl_dns)


class UploadMudView(generics.CreateAPIView):
    """
    API endpoint that allows admins to upload MUD-files
    """

    queryset = MUD.nodes.all()
    serializer_class = UploadMudSerializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        # try:
        json_data = json.loads(request.data["serial"])
        parse = MudParser(json_data)
        response = {"result": parse.result}
        return JsonResponse(response, safe=False)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    )
)
class ServicesByThingView(generics.ListAPIView):
    """
    API endpoint that shows which Services a Thing can access
    """

    serializer_class = ServiceSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        """All Database operations should be defined in our models.
        This is why there is a function called to_services in mudapi.model.thing.Thing
        The query needs to be called with the parameter mac-addr, to that we can identify the Thing
        Django restframe work always works with expr as alias:
        querysets. here we overridde the default queryset.
        According to drf documentation it is necesarry, that is is a function, otherwise the results will
        be cached of prior requests.
        """
        t = Thing.nodes.get(mac_addr=self.request.query_params.get("mac-addr"))
        if t is None:
            raise NotFound(detail="Error 404, Thing not found", code=404)
        queryset = services_of_thing(mac_addr=self.request.query_params.get("mac-addr"))
        return queryset


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    )
)
class ThingsOfAManagerView(generics.ListAPIView):
    """Returns a list of all Things, which are managed by a specific Manager"""

    serializer_class = ThingSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        ip_external = self.request.query_params.get("ip-external")
        data = {"ip": ip_external}
        IPSerializer(data=data).is_valid(raise_exception=True)
        # TODO do we need to check if this device exists, to solve 500 Errors
        return things_of_a_manager(ip_external)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="schema",
                description="The schema which will be in the returned mud files",
                required=True,
                type=str,
            )
        ]
    )
)
class FindMUDBySchemaView(generics.ListAPIView):
    """Return all MUD Files which allow a specific network Protocol schema (ssh, ipp, etc)"""

    serializer_class = MUDSerializer
    permissions_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        schema = self.request.query_params.get("schema")
        return mud_from_schema(schema=schema)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="port_start",
                description="This is where the port range starts",
                required=True,
                type=int,
            ),
            OpenApiParameter(
                name="port_end",
                description="This is where the port range ends",
                required=False,
                type=int,
            ),
        ]
    )
)
class FindMUDByPortView(generics.ListAPIView):
    """Return all MUD Files which allow connections to a specific port"""

    serializer_class = MUDSerializer
    permissions_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):

        start = self.request.query_params.get("port_start")
        end = self.request.query_params.get("port_end")
        try:
            port_start = int(start)
        except ValueError:
            raise ValidationError("port_start is not an int")
        if "port_end" not in self.request.query_params:
            port_end = port_start
        else:
            try:
                port_end = int(end)
            except ValueError:
                raise ValidationError("port_end not an int")

        if port_end < port_start:
            logger.error("start is bigger than end")
            raise ValidationError("port_start is bigger than port_end")
        if port_end < 0 or port_start < 0:
            raise ValidationError("Ports can only be positive")
        if port_end > 65535.0 or port_start > 65535.0:
            raise ValidationError("Ports can only be positive")

        ports = list(range(port_start, port_end + 1))
        return mud_from_port(ports)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    )
)
class FindThingsByMacVendor(generics.ListAPIView):
    """
    Returns all know devices of a specified mac vendor
    Errata: This is broken if the Thing uses mac adress randomisation (Smartphones)
    """

    serializer_class = ThingSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        mac_addr = self.request.query_params.get("mac-addr")
        t = Thing.nodes.get(mac_addr=mac_addr)
        if t is None:
            raise NotFound(detail="Error 404, Thing not found", code=404)
        vendor = MacLookup().lookup(mac_addr)
        return thing_from_mac_vendor(vendor)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    )
)
class GuessMUDByThingsConnections(generics.ListAPIView):
    """
    Guesses which MUD file belongs to a Thing by analysing its connections
    """

    serializer_class = MUDSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        mac_addr = self.request.query_params.get("mac-addr")
        t = Thing.nodes.get(mac_addr=mac_addr)
        if t is None:
            raise NotFound(detail="Error 404, Thing not found", code=404)
        return mud_from_thing(mac_addr)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    ),
    post=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    ),
)
class ThingConnections(generics.ListCreateAPIView):
    """
    Create or List connections (ACL Relationsships for Things)
    """

    serializer_class = ACLSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        mac_addr = self.request.query_params.get("mac-addr")
        t = Thing.nodes.get_or_none(mac_addr=mac_addr)
        if t is None:
            raise NotFound(detail="Error 404, Thing not found", code=404)
        ac = connections_of_thing(mac_addr)
        return ac

    def perform_create(self, serializer):
        mac_addr = self.request.query_params.get("mac-addr")
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        t = Thing.nodes.get_or_none(mac_addr=mac_addr)
        service_name = get_service_name(self.request.data["acl_dns"])
        service = Service.nodes.first_or_none(name=service_name)
        if service is None:
            service = Service(name=service_name).save()
        if t is None:
            raise NotFound(detail="Error 404, Thing not found", code=404)
        t.acl.connect(
            service,
            serializer.data,
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mud_url",
                description="The mud url of the MUD",
                required=True,
                type=str,
            )
        ]
    ),
    post=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mud_url",
                description="The mud url of the MUD",
                required=True,
                type=str,
            )
        ]
    ),
)
class MUDConnections(generics.ListCreateAPIView):
    """
    Create or List connections (ACL Relationsships for MUDs)
    """

    serializer_class = ACLSerializer
    permissions_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        mud_url = self.request.query_params.get("mud_url")
        t = MUD.nodes.get(mud_url=mud_url)
        if t is None:
            raise NotFound(detail="Error 404, MUD not found", code=404)
        ac = connections_of_mud(mud_url)
        return ac

    def perform_create(self, serializer):
        mud_url = self.request.query_params.get("mud_url")
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        t = MUD.nodes.get_or_none(mud_url=mud_url)
        service_name = get_service_name(mud_url)
        service = Service.nodes.first_or_none(name=service_name)
        if service is None:
            service = Service(name=service_name).save()
        if t is None:
            raise NotFound(detail="Error 404, MUD not found", code=404)
        t.acl.connect(
            service,
            serializer.data,
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    )
)
class ThingAllowedConnections(generics.ListAPIView):
    """
    List of allow Connections allowed for a Thing specified in its MUD
    """

    serializer_class = ACLSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        mac_addr = self.request.query_params.get("mac-addr")
        t = Thing.nodes.get(mac_addr=mac_addr)
        if t is None:
            raise NotFound(detail="Error 404, Thing not found", code=404)
        ac = allowed_connections_of_thing(mac_addr)
        return ac


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mud_url",
                description="The mud-url for which the Things are shown",
                required=True,
                type=str,
            )
        ]
    ),
)
class MUDOfAThing(generics.ListAPIView):
    """
    All Things which are described by a MUD
    """

    serializer_class = ThingSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        mud_url = self.request.query_params.get("mud_url")
        m = MUD.nodes.get_or_none(mud_url=mud_url)
        if m is None:
            raise NotFound(detail="Error 404, MUD not found", code=404)
        return mud_of_thing(m.mud_url)


@extend_schema_view(
    post=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mac-addr",
                description="The mac adress of the Thing",
                required=True,
                type=str,
            )
        ]
    )
)
class ThingMUD(generics.CreateAPIView):
    """
    POST: Things is described by the specified MUD
    """

    serializer_class = DescriptionSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        t = Thing.nodes.get_or_none(mac_addr=serializer.data.get("mac_addr"))
        m = MUD.nodes.get_or_none(mud_url=serializer.data.get("mud_url"))
        if t is None:
            raise NotFound(detail="Error 404, Thing not found", code=404)
        if m is None:
            raise NotFound(detail="Error 404, MUD not found", code=404)
        t.mud.connect(m)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@extend_schema_view(
    get=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mud_url",
                description="The mud-url for which the Things are shown",
                required=True,
                type=str,
            )
        ]
    ),
)
class JSONOfAMUD(generics.ListAPIView):
    """
    JSON file of a MUD URL, this can be newer than the file served at the URL
    """

    serializer_class = MUDJsonSerializer
    permissions_classes = [permissions.IsAuthenticatedOrReadOnly]

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        mud_url = self.request.query_params.get("mud_url")
        m = MUD.nodes.get_or_none(mud_url=mud_url)
        if m is None:
            raise NotFound(detail="Error 404, MUD not found", code=404)
        try:
            # res = pprint.pformat(serialize_mud(m))
            res = json.dumps(serialize_mud(m))
        finally:
            return Response(res)


@extend_schema_view(
    post=extend_schema(
        parameters=[
            OpenApiParameter(
                name="mud_url",
                description="The mud-url for which the Things are shown",
                required=True,
                type=str,
            ),
            OpenApiParameter(
                name="name",
                description="name of the file which has been changed and for which you want to create a merge request",
                required=True,
                type=str,
            )
        ]
    ),
)
class CreateFork(generics.CreateAPIView):
    """
    Creates a Fork on our Gitlab Instance so that the changes can be merged by a maintainer
    """
    serializer_class = ForkSerializer
    permissions_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        # Note the use of `get_queryset()` instead of `self.queryset`
        mud_url = self.request.query_params.get("mud_url")
        name = self.request.query_params.get("name")
        m = MUD.nodes.get_or_none(mud_url=mud_url)
        if m is None:
            raise NotFound(detail="Error 404, MUD not found", code=404)
        try:
            save_mud_gitlab(m, name)
        except Exception: # Todo improve error messages
            return Response("Failed to create your fork", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        finally:
            return Response("Fork has been created", status=status.HTTP_201_CREATED)


class UploadPcapMudGeneratorViewSet(generics.CreateAPIView):
    """
    Create New Mud from pcap data
    """
    serializer_class = GeneratorSerializer
    permissions_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        file_uploaded = request.data['file']
        content_type = file_uploaded.content_type
        path = default_storage.save('./mudgee/' + file_uploaded.name, ContentFile(file_uploaded.read()))
        return JsonResponse(generate_mud(request.data['gateway_mac'], request.data['gateway_ipv4'],
                            request.data['gateway_ipv6'], request.data['device_mac'],
                                         request.data['device_name'], path), safe=False)
