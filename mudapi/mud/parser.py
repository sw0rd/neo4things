from neomodel import db

from mudapi.models.manufacturer import Manufacturer
from mudapi.models.mud import MUD
from mudapi.models.service import Service
# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__file__)



class MudParser:
    """
    Helper Class for MUD serialization
    """

    def find_manufacturer_or_create(self, name):
        try:
            return Manufacturer.nodes.filter(name=name)[0]
        except:
            return Manufacturer.create({"name": name})[0].save()

    def find_service_or_create(self, name):
        try:
            return Service.nodes.filter(name=name)[0]
        except:
            return Service.create({"name": name})[0].save()

    def find_mud_and_delete(self, url):
        db.cypher_query(
            "match (m:MUD {mud_url: $mud_url})-[r]->(o) delete r", {"mud_url": url}
        )
        db.cypher_query(
            "match (m:MUD {mud_url: $mud_url})<-[r]-(o) delete r", {"mud_url": url}
        )
        db.cypher_query("match (m:MUD {mud_url: $mud_url}) delete m", {"mud_url": url})

    def get_or_create_dns_ntp_service(self):
        return [
            self.find_service_or_create("dns_service"),
            self.find_service_or_create("ntp_service"),
        ]

    def __init__(self, js):
        self.muds = {}
        self.acls_entries = {}
        self.result = []

        for entry in js.keys():
            name, ext = entry.split(":")
            if ext == "mud":
                self.muds[name] = js[entry]
            elif ext == "acls" or ext == "access-lists":
                for acl in js[entry]["acl"]:
                    self.parse_acl(acl)
            else:
                raise BaseException("invalid MUD type: " + ext)

        self.create_muds()

    def parse_acl(self, acl):
        acl_entries = []
        for ace in acl["aces"]["ace"]:
            matches_ip = []
            matches_tcp = []
            matches_udp = []
            for match in ace["matches"]:
                if match == "ipv6":
                    for entry in ace["matches"][match]:
                        if (
                            entry.split(":")[-1] == "src-dnsname"
                            or entry.split(":")[-1] == "dst-dnsname"
                        ):
                            tmp = {}
                            tmp["dns"] = ace["matches"][match][entry]
                            tmp["version"] = 6
                            matches_ip.append(tmp)
                elif match == "ipv4":
                    for entry in ace["matches"][match]:
                        if (
                            entry.split(":")[-1] == "src-dnsname"
                            or entry.split(":")[-1] == "dst-dnsname"
                        ):
                            tmp = {}
                            tmp["dns"] = ace["matches"][match][entry]
                            tmp["version"] = 4
                            matches_ip.append(tmp)
                elif match == "tcp" or match == "udp":
                    info = ace["matches"][match]
                    match_conn = {}
                    match_conn["direction"] = "unspecified"
                    for entry in info:
                        if entry.split(":")[-1] == "direction-initiated":
                            match_conn["direction"] = info[entry]

                    if "source-port" in info:
                        port = info["source-port"]
                    elif "destination-port" in info:
                        port = info["destination-port"]
                    else:
                        logger.warning("WARN: no ACL port specified for TCP; Assuming all allowed")
                        port = {'operator': 'range', 'lower-port': 0, 'upper-port': 65535}

                    if port["operator"] == "eq" and "lower-port" in port and "upper-port" in port and not "port" in port:
                        logger.warning("WARN: non-standard port setup for operator 'eq'; Assuming 'range' was meant")
                        match_conn["port"] = list(
                            range(port["lower-port"], port["upper-port"])
                        )
                        if match == "tcp":
                            matches_tcp.append(match_conn)
                        else:
                            matches_udp.append(match_conn)
                        
                    if port["operator"] == "eq":
                        if "port" in port:
                            match_conn["port"] = [port["port"]]
                        if match == "tcp":
                            matches_tcp.append(match_conn)
                        else:
                            matches_udp.append(match_conn)
                    elif port["operator"] == "range":
                        match_conn["port"] = list(
                            range(port["lower-port"], port["upper-port"])
                        )
                        if match == "tcp":
                            matches_tcp.append(match_conn)
                        else:
                            matches_udp.append(match_conn)
                    else:
                        raise BaseException(
                            "invalid ACE port operator: " + port["operator"]
                        )

                else:
                    logger.warning("invalid ACE match type: " + match)
                    # raise BaseException('invalid ACE match type: ' + match)

            for action in ace["actions"]:
                if action == "forwarding":
                    act = ace["actions"][action]
                    for tcp in matches_tcp:
                        for ip in matches_ip:
                            con = {}
                            con["protocol"] = ip["version"]
                            con["data"] = {}
                            con["data"]["type"] = str(ip["version"]) + "t"
                            con["data"]["acl_dns"] = ip["dns"]
                            con["data"]["port"] = tcp["port"]
                            con["data"]["direction_initiated"] = tcp["direction"]
                            con["data"]["forwarding"] = act
                            acl_entries.append(con)
                    for udp in matches_udp:
                        for ip in matches_ip:
                            con = {}
                            con["protocol"] = ip["version"]
                            con["data"] = {}
                            con["data"]["type"] = str(ip["version"]) + "u"
                            con["data"]["acl_dns"] = ip["dns"]
                            con["data"]["port"] = udp["port"]
                            con["data"]["direction_initiated"] = udp["direction"]
                            con["data"]["forwarding"] = act
                            acl_entries.append(con)

        self.acls_entries[acl["name"]] = acl_entries

    def create_muds(self):
        for mud_name in self.muds:
            mud = self.muds[mud_name]

            self.find_mud_and_delete(url=mud["mud-url"])

            if not "signature" in mud:
                mud[
                    "signature"
                ] = "no signature"  # TODO: what is the default signature?

            manufacturer_name = mud["mud-url"].split("/")[2]  # TLD per specification
            manufacturer = self.find_manufacturer_or_create(name=manufacturer_name)

            mud_name = mud["mud-url"].split("/")[-1]  # URL dest per specification
            service_name = mud_name + " service"

            dns_service, ntp_service = self.get_or_create_dns_ntp_service()
            service = self.find_service_or_create(name=service_name)
            mudInstance = MUD(
                mud_version=mud["mud-version"],
                mud_url=mud["mud-url"],
                mud_signature=mud["signature"],
                systeminfo=mud["systeminfo"],
                name=mud_name,
                firmware_rev="",  # TODO: where is this?
                software_rev="",  # TODO: where is this?
                documentation=mud["mud-url"]
                + "/documentation",  # TODO: where is the documentation
            ).save()

            mudInstance.operator.connect(manufacturer)

            if "from-device-policy" in mud:
                for kw_acl in mud["from-device-policy"]["access-lists"]["access-list"]:
                    if not kw_acl["name"] in self.acls_entries:
                        raise BaseException("ACL not in MUD: " + kw_acl["name"])
                    for acl in self.acls_entries[kw_acl["name"]]:
                        mudInstance.acl.connect(service, acl["data"])
            if "to-device-policy" in mud:
                for kw_acl in mud["to-device-policy"]["access-lists"]["access-list"]:
                    if not kw_acl["name"] in self.acls_entries:
                        raise BaseException("ACL not in MUD: " + acl["name"])
                    for acl in self.acls_entries[kw_acl["name"]]:
                        mudInstance.acl.connect(service, acl["data"])

            mudInstance.save()
            self.result.append(mudInstance.mud_url)
