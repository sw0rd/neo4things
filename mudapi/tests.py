import unittest

from rest_framework.test import APITestCase

# Create your tests here.
from django.contrib.auth.models import User
from neomodel import clear_neo4j_database
from rest_framework import status

# Using the standard RequestFactory API to create a form POST request
from rest_framework.test import APIRequestFactory, APIClient, CoreAPIClient, force_authenticate

import mudapi
from neomodel import db

# https://www.django-rest-framework.org/api-guide/testing/
# https://www.django-rest-framework.org/api-guide/testing/#forcing-authentication
from mudapi.models import MUD, Thing, Service
from mudapi.serializers import MUDSerializer, ThingSerializer
from mudapi.views import UserViewSet, MUDViewSet, ThingViewSet

from mudapi.models.queries import (
    connections_of_thing,
)

import json


class AuthAPITestCase(APITestCase):
    def setUp(self):
        try:
            user = User.objects.get(name='admin2')
        except:
            user = User.objects.create_user('admin2', 'admin@a.de', 'admin2')
        self.client.force_authenticate(user=user)
        clear_neo4j_database(db)  # deletes all nodes and relationships
        if hasattr(self, "initData"):
            self.initData()

    def assertPost(self, code, url, **kwargs):
        request = self.client.post(url, kwargs)
        if not request.status_code == code:
            self.assertEqual("invalid status code: " + str(request.status_code) + ", expected " + str(code) + " ", str(request.content))
        self.assertEqual(request.status_code, code)
        return request


class GetAllMudTest(AuthAPITestCase):
    """ Test module for GET all MUD API """

    def initData(self):  # will be run first
        MUD(mud_version=1,
            mud_url="https://lighting.example.com/lightbulb2000",
            mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
            systeminfo="The BMS Example Light Bulb",
            name="Bar MUD",  # this can be found form the mud url after the domain
            firmware_rev="0.1",  # Liam
            software_rev="0.1",  # Liam
            documentation="https://lighting.example.com/documentation",
            ).save()

    def test_mud(self):
        response = self.client.get('/mud/')
        MUDs = MUD.nodes.all()  # Use neomodell to read from db direct
        serializer = MUDSerializer(MUDs, many=True)  # Die Daten der Datenbank serializisieren
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], serializer.data)

    def test_empty_mud(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships

        response = self.client.get('/mud/')

        MUDs = MUD.nodes.all()  # Use neomodell to read from db direct
        serializer = MUDSerializer(MUDs, many=True)  # Die Daten der Datenbank serializisieren
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], serializer.data)


class GetAllTHINGTest(AuthAPITestCase):
    """ Test module for GET all THINGS API """

    def initData(self):  # will be run first
        Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()
        Thing(mac_addr="65:CC:22:3A:AC:BE", ipv4_addr="192.168.2.2", hostname="ligt2.lan").save()

    def test_thing(self):
        response = self.client.get('/thing/')
        THINGs = Thing.nodes.all()  # Use neomodell to read from db direct
        serializer = ThingSerializer(THINGs, many=True)  # Die Daten der Datenbank serializisieren
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], serializer.data)

    def test_thing_empty(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships
        response = self.client.get('/thing/')
        THINGs = Thing.nodes.all()  # Use neomodell to read from db direct
        serializer = ThingSerializer(THINGs, many=True)  # Die Daten der Datenbank serializisieren
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], serializer.data)

class GetTHINGTest(AuthAPITestCase):
    """ Test module for GET THING API """

    def initData(self):  # will be run first
        self.t = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()

    def test_get(self):
        response = self.client.get('/thing/64:CC:22:3A:AC:BE/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content)['serial'], self.t.serial)
        self.assertEqual(json.loads(response.content)['mac_addr'], self.t.mac_addr)
        


class PostTHINGTest(AuthAPITestCase):
    """ Test module for POST THING API """

    def test_mud(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships

        response = self.client.post('/thing/',
                                    {'serial': '1234', 'mac_addr': '64:CC:22:3A:AC:BE', 'ipv4_addr': '192.168.2.1',
                                     'ipv6_addr': 'fe::1', 'hostname': 'light.lan'})

        THINGs = Thing.nodes.all()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(THINGs), 1)
        self.assertEqual(THINGs[0].serial, "1234")
        self.assertEqual(THINGs[0].ipv4_addr, "192.168.2.1")
        self.assertEqual(THINGs[0].ipv6_addr, "fe::1")
        self.assertEqual(THINGs[0].hostname, "light.lan")
        self.assertEqual(THINGs[0].mac_addr, "64:CC:22:3A:AC:BE")

    def test_mud_duid(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships

        response = self.client.post('/thing/',
                                    {'serial': '1234', 'mac_addr': '00:02:00:01:02:03:04:05:07:a0', 'ipv4_addr': '192.168.2.1',
                                     'ipv6_addr': 'fe::1', 'hostname': 'light.lan'})

        THINGs = Thing.nodes.all()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(THINGs), 1)
        self.assertEqual(THINGs[0].serial, "1234")
        self.assertEqual(THINGs[0].ipv4_addr, "192.168.2.1")
        self.assertEqual(THINGs[0].ipv6_addr, "fe::1")
        self.assertEqual(THINGs[0].hostname, "light.lan")
        self.assertEqual(THINGs[0].mac_addr, "00:02:00:01:02:03:04:05:07:a0")

    def test_invalid_base(self, **kwargs):
        self.assertPost(status.HTTP_400_BAD_REQUEST, "/thing/", **kwargs)

    def test_invalid_serial(self):
        self.test_invalid_base(
            serial='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            mac_addr='64:CC:22:3A:AC:BE',
            ipv4_addr='192.168.2.1',
            ipv6_addr='fe::1',
            hostname='light.lan'
        )

    def test_invalid_mac(self):
        self.test_invalid_base(
            serial='1234',
            mac_addr='64:CC:22:3A:AC::BE',
            ipv4_addr='192.168.2.1',
            ipv6_addr='fe::1',
            hostname='light.lan'
        )


    def test_invalid_ipv4(self):
        self.test_invalid_base(
            serial='1234',
            mac_addr='64:CC:22:3A:AC:BE',
            ipv4_addr='invalid',
            ipv6_addr='fe::1',
            hostname='light.lan'
        )
        self.test_invalid_base(
            serial='1234',
            mac_addr='64:CC:22:3A:AC:BE',
            ipv4_addr='abc:def:ghji1',
            ipv6_addr='fe::1',
            hostname='light.lan'
        )

    def test_invalid_ipv6(self):
        self.test_invalid_base(
            serial='1234',
            mac_addr='64:CC:22:3A:AC:BE',
            ipv4_addr='192.168.2.1',
            ipv6_addr='invalid',
            hostname='light.lan'
        )
        self.test_invalid_base(
            serial='1234',
            mac_addr='64:CC:22:3A:AC:BE',
            ipv4_addr='192.168.2.1',
            ipv6_addr='192.168.2.1::fe',
            hostname='light.lan'
        )

    def test_invalid_hostname(self):
        self.test_invalid_base(
            serial='1234',
            mac_addr='64:CC:22:3A:AC:BE',
            ipv4_addr='192.168.2.1',
            ipv6_addr='fe::1',
            hostname='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
        )


class PostMUDTestCase(AuthAPITestCase):

    def test_post_valid(self):
        valid_mud = """
{
     "ietf-mud:mud": {
       "mud-version": 1,
       "mud-url": "https://lighting.example.com/lightbulb2000",
       "last-update": "2019-01-28T11:20:51+01:00",
       "cache-validity": 48,
       "is-supported": true,
       "systeminfo": "The BMS Example Light Bulb",
       "from-device-policy": {
         "access-lists": {
           "access-list": [
             {
               "name": "mud-76100-v6fr"
             }
           ]
         }
       },
       "to-device-policy": {
         "access-lists": {
           "access-list": [
             {
               "name": "mud-76100-v6to"
             }
           ]
         }
       }
     },
     "ietf-access-control-list:acls": {
       "acl": [
         {
           "name": "mud-76100-v6to",
           "type": "ipv6-acl-type",
           "aces": {
             "ace": [
               {
                 "name": "cl0-todev",
                 "matches": {
                   "ipv6": {
                     "ietf-acldns:src-dnsname": "test.example.com",
                     "protocol": 6
                   },
                   "tcp": {
                     "ietf-mud:direction-initiated": "from-device",
                     "source-port": {
                       "operator": "eq",
                       "port": 443
                     }
                   }
                 },
                 "actions": {
                   "forwarding": "accept"
                 }
               }
             ]
           }
         },
         {
           "name": "mud-76100-v6fr",
           "type": "ipv6-acl-type",
           "aces": {
             "ace": [
               {
                 "name": "cl0-frdev",
                 "matches": {
                   "ipv6": {
                     "ietf-acldns:dst-dnsname": "test.example.com",
                     "protocol": 6
                   },
                   "tcp": {
                     "ietf-mud:direction-initiated": "from-device",
                     "destination-port": {
                       "operator": "eq",
                       "port": 443
                     }
                   }
                 },
                 "actions": {
                   "forwarding": "accept"
                 }
               }
             ]
           }
         }
       ]
     }
   }
        """

        clear_neo4j_database(db)  # deletes all nodes and relationships

        response = self.client.post('/mud/upload', {'serial': valid_mud})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content)['result'], ['https://lighting.example.com/lightbulb2000'])
        # TODO: Assert connections, MUD etc.


class GetMUDTestCase(AuthAPITestCase):

    def test_simple_mud(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships
        omud = MUD(mud_version=1,
                   mud_url="https://lighting.example.com/lightbulb2000",
                   mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                   systeminfo="The BMS Example Light Bulb",
                   name="Bar MUD",  # this can be found form the mud url after the domain
                   firmware_rev="0.1",  # Liam
                   software_rev="0.1",  # Liam
                   documentation="https://lighting.example.com/lightbulb2000/documentation",
                   ).save()

        response = self.client.get('/mud/json', {'mud_url': "https://lighting.example.com/lightbulb2000"})
        mud = json.loads(response.data)
        mud_data = mud['ietf-mud:mud']
        mud_acls = mud['ietf-access-control-list:acls']
        self.assertEqual(mud_acls, {'acl': []})
        self.assertEqual(mud_data['mud-version'], omud.mud_version)
        self.assertEqual(mud_data['mud-url'], omud.mud_url)
        self.assertEqual(mud_data['systeminfo'], omud.systeminfo)
        self.assertEqual(mud_data['documentation'], omud.documentation)

    def test_complex_mud(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships
        omud = MUD(mud_version=1,
                   mud_url="https://lighting.example.com/lightbulb2000",
                   mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                   systeminfo="The BMS Example Light Bulb",
                   name="Bar MUD",  # this can be found form the mud url after the domain
                   firmware_rev="0.1",  # Liam
                   software_rev="0.1",  # Liam
                   documentation="https://lighting.example.com/lightbulb2000/documentation",
                   ).save()
        omud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                         {'type': '4u', 'acl_dns': 'test-service-dns', 'port': [1, 2, 5],
                          'direction_initiated': 'from-device', 'forwarding': 'allow'})

        response = self.client.get('/mud/json', {'mud_url': "https://lighting.example.com/lightbulb2000"})
        mud = json.loads(response.data)
        mud_data = mud['ietf-mud:mud']
        mud_acls = mud['ietf-access-control-list:acls']
        # print(mud_acls)
        self.assertEqual(len(mud_acls['acl']), 1)
        self.assertEqual(mud_acls['acl'][0]['type'], 'ipv4')
        # self.assertEqual(len(mud_acls['acl'][0]['aces']['ace']), 1) # Why does this create so many ACEs?
        self.assertEqual(mud_data['mud-version'], omud.mud_version)
        self.assertEqual(mud_data['mud-url'], omud.mud_url)
        self.assertEqual(mud_data['systeminfo'], omud.systeminfo)
        self.assertEqual(mud_data['documentation'], omud.documentation)

    def test_reproduce_mud(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships
        omud = MUD(mud_version=1,
                   mud_url="https://lighting.example.com/lightbulb2000",
                   mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                   systeminfo="The BMS Example Light Bulb",
                   name="Bar MUD",  # this can be found form the mud url after the domain
                   firmware_rev="0.1",  # Liam
                   software_rev="0.1",  # Liam
                   documentation="https://lighting.example.com/lightbulb2000/documentation",
                   ).save()
        omud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                         {'type': '4u', 'acl_dns': 'test-service-dns', 'port': [1, 2, 5],
                          'direction_initiated': 'from-device', 'forwarding': 'allow'})

        response = self.client.get('/mud/json', {'mud_url': "https://lighting.example.com/lightbulb2000"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        clear_neo4j_database(db)
        response2 = self.client.post('/mud/upload', {'serial': response.data})
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response2.content)['result'], ['https://lighting.example.com/lightbulb2000'])

        nmud = MUD.nodes.all()
        self.assertEqual(len(nmud), 1)
        self.assertEqual(nmud[0].systeminfo, omud.systeminfo)
        self.assertEqual(nmud[0].documentation, omud.documentation)

        conns = nmud[0].acl
        # self.assertEqual(len(conns), 1)
        # TODO: FIX serializer, then continue here


class GetThingConnectionsTestCase(AuthAPITestCase):
    def initData(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships
        self.th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()
        self.th.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                            {'type': '4u', 'acl_dns': 'test-service-dns', 'port': [1, 2, 5],
                             'direction_initiated': 'from-device', 'forwarding': 'allow'})

    def validate_acls_result(self, res):
        results = connections_of_thing(self.th.mac_addr)
        self.assertEqual(len(res.data['results']), len(results))
        for i in range(0, len(results)):
            self.assertEqual(res.data['results'][i]['type'], results[i].type)
            self.assertEqual(res.data['results'][i]['acl_dns'], results[i].acl_dns)
            self.assertEqual(res.data['results'][i]['port'], results[i].port)
            self.assertEqual(res.data['results'][i]['direction_initiated'], results[i].direction_initiated)
            self.assertEqual(res.data['results'][i]['forwarding'], results[i].forwarding)

    def test_get_acls(self):
        response = self.client.get('/thing/connections', {'mac-addr': self.th.mac_addr})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.validate_acls_result(res=response)

    def test_get_acls_no_thing(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships
        response = self.client.get('/thing/connections', {'mac-addr': self.th.mac_addr})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_acls_empty(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships
        self.th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()
        response = self.client.get('/thing/connections', {'mac-addr': self.th.mac_addr})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.validate_acls_result(res=response)


class PostThingConnectionsTestCase(AuthAPITestCase):
    def test_success(self):
        clear_neo4j_database(db)  # deletes all nodes and relationships

        th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()
        response = self.client.post('/thing/connections?mac-addr=' + th.mac_addr,
                                    {'name': "test", 'type': "4u", 'acl_dns': "test.dns", 'port': [1, 2, 3, 4],
                                     'direction_initiated': "from-device", 'forwarding': 'accept',
                                     'timestamp': "2021-03-17T16:34:59.609000Z"}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.get('/thing/connections', {'mac-addr': th.mac_addr})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['count'], 1)
        result = response.data['results'][0]

        self.assertEqual(result['type'], '4u')
        self.assertEqual(result['acl_dns'], 'test.dns')
        self.assertEqual(result['port'], [1, 2, 3, 4])
        self.assertEqual(result['direction_initiated'], 'from-device')
        self.assertEqual(result['forwarding'], 'accept')
        # self.assertContains (result['timestamp'], '202') TODO timestap fix

    def test_invalid_base(self, **kwargs):
        th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()
        self.assertPost(status.HTTP_400_BAD_REQUEST, '/thing/connections?mac-addr=' + th.mac_addr, **kwargs)

    def test_invalid_name(self):
        self.test_invalid_base(
            name='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            type='4u',
            acl_dns='test.dns',
            port=[1],
            direction_initiated='from-device',
            forwarding='accept',
            timestamp='2021-03-17T16:34:59.609000Z'
        )

    def test_invalid_type(self):
        self.test_invalid_base(
            name='test',
            type='4p',
            acl_dns='test.dns',
            port=[1],
            direction_initiated='from-device',
            forwarding='accept',
            timestamp='2021-03-17T16:34:59.609000Z',
        )

    def test_invalid_port(self):
        self.test_invalid_base(
            name='test',
            type='invalid',
            acl_dns='test.dns',
            port=[],
            direction_initiated='from-device',
            forwarding='accept',
            timestamp='2021-03-17T16:34:59.609000Z',
        )


class GuessThingTestCase(AuthAPITestCase):
    def initData(self):
        clear_neo4j_database(db)
        mud = MUD(mud_version=1,
                  mud_url="https://lighting.example.com/lightbulb2000",
                  mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                  systeminfo="The BMS Example Light Bulb",
                  name="Bar MUD",  # this can be found form the mud url after the domain
                  firmware_rev="0.1",  # Liam
                  software_rev="0.1",  # Liam
                  documentation="https://lighting.example.com/lightbulb2000/documentation",
                  ).save()
        mud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                        {'type': '4u', 'acl_dns': 'test-service-dns', 'port': [1, 2, 5],
                         'direction_initiated': 'from-device', 'forwarding': 'allow'})

        mud = MUD(mud_version=1,
                  mud_url="https://lighting.example.com/lightbulb2001",
                  mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                  systeminfo="The BMS Example Light Bulb",
                  name="Bar MUD",  # this can be found form the mud url after the domain
                  firmware_rev="0.1",  # Liam
                  software_rev="0.1",  # Liam
                  documentation="https://lighting.example.com/lightbulb2001/documentation",
                  ).save()

        mud = MUD(mud_version=1,
                  mud_url="https://lighting.example.com/lightbulb2002",
                  mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                  systeminfo="The BMS Example Light Bulb",
                  name="Bar MUD",  # this can be found form the mud url after the domain
                  firmware_rev="0.1",  # Liam
                  software_rev="0.1",  # Liam
                  documentation="https://lighting.example.com/lightbulb2001/documentation",
                  ).save()
        mud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                        {'type': '4u', 'acl_dns': 'test-service-dns', 'port': [2, 5],
                         'direction_initiated': 'from-device', 'forwarding': 'allow'})
        mud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                        {'type': '6u', 'acl_dns': 'test-service-2-dns', 'port': [99, 125],
                         'direction_initiated': 'from-device', 'forwarding': 'allow'})

    def expect_mud(self, result, url):
        for res in result.data['results']:
            if res['mud_url'] == url:
                return
        self.assertEqual("", "mud url [" + url + "] not in results")

    def test_dns_only(self):
        self.initData()
        self.assertPost(status.HTTP_201_CREATED, "/thing/", serial="test", mac_addr="64:CC:22:3A:AC:BE",
                        ipv4_addr="192.168.2.1", ipv6_addr="fe::f0", hostname="test.thing")
        self.assertPost(status.HTTP_201_CREATED, "/thing/connections?mac-addr=64:CC:22:3A:AC:BE", name="test",
                        type="4u", acl_dns="test-service-dns", port=[1], direction_initiated="from-device",
                        forwarding="accept", timestamp="2021-03-17T16:34:59.609000Z")

        request = self.client.get("/mud/guessThing", {'mac-addr': '64:CC:22:3A:AC:BE'})
        self.assertEqual(request.data['count'], 2)
        self.expect_mud(request, "https://lighting.example.com/lightbulb2000")
        self.expect_mud(request, "https://lighting.example.com/lightbulb2002")

    def test_no_connection(self):
        self.initData()
        self.assertPost(status.HTTP_201_CREATED, "/thing/", serial="test", mac_addr="64:CC:22:3A:AC:BE",
                        ipv4_addr="192.168.2.1", ipv6_addr="fe::f0", hostname="test.thing")

        request = self.client.get("/mud/guessThing", {'mac-addr': '64:CC:22:3A:AC:BE'})
        self.assertEqual(request.data['count'], 0)

    def test_no_match(self):
        self.initData()
        self.assertPost(status.HTTP_201_CREATED, "/thing/", serial="test", mac_addr="64:CC:22:3A:AC:BE",
                        ipv4_addr="192.168.2.1", ipv6_addr="fe::f0", hostname="test.thing")
        self.assertPost(status.HTTP_201_CREATED, "/thing/connections?mac-addr=64:CC:22:3A:AC:BE", name="test",
                        type="4u", acl_dns="some-other-service", port=[1], direction_initiated="from-device",
                        forwarding="accept", timestamp="2021-03-17T16:34:59.609000Z")

        request = self.client.get("/mud/guessThing", {'mac-addr': '64:CC:22:3A:AC:BE'})
        self.assertEqual(request.data['count'], 0)

    # TODO: These tests only work if matching by port is set
    # def test_success(self):
    #     self.initData()
    #     self.assertPost(status.HTTP_201_CREATED, "/thing/", serial="test", mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", ipv6_addr="fe::f0", hostname="test.thing")
    #     self.assertPost(status.HTTP_201_CREATED, "/thing/connections?mac-addr=64:CC:22:3A:AC:BE", name="test", type="4u", acl_dns="test-service-dns", port=[1], direction_initiated="from-device", forwarding="accept", timestamp="2021-03-17T16:34:59.609000Z")

    #     request = self.client.get("/mud/guessThing", {'mac-addr': '64:CC:22:3A:AC:BE' })
    #     self.assertEqual(request.data['count'], 1)
    #     self.assertEqual(request.data['results'][0]['mud_url'], "https://lighting.example.com/lightbulb2000")

    # def test_success2(self):
    #     self.initData()
    #     self.assertPost(status.HTTP_201_CREATED, "/thing/", serial="test", mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", ipv6_addr="fe::f0", hostname="test.thing")
    #     self.assertPost(status.HTTP_201_CREATED, "/thing/connections?mac-addr=64:CC:22:3A:AC:BE", name="test", type="4u", acl_dns="test-service-dns", port=[1, 2, 5], direction_initiated="from-device", forwarding="accept", timestamp="2021-03-17T16:34:59.609000Z")

    #     request = self.client.get("/mud/guessThing", {'mac-addr': '64:CC:22:3A:AC:BE' })
    #     self.assertEqual(request.data['count'], 1)
    #     self.assertEqual(request.data['results'][0]['mud_url'], "https://lighting.example.com/lightbulb2000")

    # def test_multiple_matches(self):
    #     self.initData()
    #     self.assertPost(status.HTTP_201_CREATED, "/thing/", serial="test", mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", ipv6_addr="fe::f0", hostname="test.thing")
    #     self.assertPost(status.HTTP_201_CREATED, "/thing/connections?mac-addr=64:CC:22:3A:AC:BE", name="test", type="4u", acl_dns="test-service-dns", port=[5], direction_initiated="from-device", forwarding="accept", timestamp="2021-03-17T16:34:59.609000Z")
    #     self.assertPost(status.HTTP_201_CREATED, "/thing/connections?mac-addr=64:CC:22:3A:AC:BE", name="test", type="4u", acl_dns="test-service-dns", port=[2], direction_initiated="from-device", forwarding="accept", timestamp="2021-03-17T16:34:59.609000Z")

    #     request = self.client.get("/mud/guessThing", {'mac-addr': '64:CC:22:3A:AC:BE' })
    #     self.assertEqual(request.data['count'], 2)


class ThingDescribeTestCase(AuthAPITestCase):
    def initData(self):
        clear_neo4j_database(db)
        mud = MUD(mud_version=1,
                  mud_url="https://lighting.example.com/lightbulb2000",
                  mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                  systeminfo="The BMS Example Light Bulb",
                  name="Bar MUD",  # this can be found form the mud url after the domain
                  firmware_rev="0.1",  # Liam
                  software_rev="0.1",  # Liam
                  documentation="https://lighting.example.com/lightbulb2000/documentation",
                  ).save()
        mud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                        {'type': '4u', 'acl_dns': 'test-service-dns', 'port': [1, 2, 5],
                         'direction_initiated': 'from-device', 'forwarding': 'allow'})

        mud = MUD(mud_version=1,
                  mud_url="https://lighting.example.com/lightbulb2001",
                  mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                  systeminfo="The BMS Example Light Bulb",
                  name="Bar MUD",  # this can be found form the mud url after the domain
                  firmware_rev="0.1",  # Liam
                  software_rev="0.1",  # Liam
                  documentation="https://lighting.example.com/lightbulb2001/documentation",
                  ).save()

        mud = MUD(mud_version=1,
                  mud_url="https://lighting.example.com/lightbulb2002",
                  mud_signature="NoOneHasSignedThis",  ## added later when mud file is signed
                  systeminfo="The BMS Example Light Bulb",
                  name="Bar MUD",  # this can be found form the mud url after the domain
                  firmware_rev="0.1",  # Liam
                  software_rev="0.1",  # Liam
                  documentation="https://lighting.example.com/lightbulb2001/documentation",
                  ).save()
        mud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                        {'type': '4u', 'acl_dns': 'test-service-dns', 'port': [2, 5],
                         'direction_initiated': 'from-device', 'forwarding': 'allow'})
        mud.acl.connect(Service.create({'name': 'test-service'})[0].save(),
                        {'type': '6u', 'acl_dns': 'test-service-2-dns', 'port': [99, 125],
                         'direction_initiated': 'from-device', 'forwarding': 'allow'})

    def test_dns_only(self):
        self.initData()
        th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()

        request = self.client.post("/thing/describe", {'mac_addr': '64:CC:22:3A:AC:BE',
                                                       'mud_url': 'https://lighting.example.com/lightbulb2000'})
        self.assertEqual(len(th.mud.all()), 1)
        self.assertEqual(th.mud.all()[0].mud_url, "https://lighting.example.com/lightbulb2000")

    def test_invalid_mud(self):
        self.initData()
        th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()

        self.assertPost(status.HTTP_404_NOT_FOUND, "/thing/describe", mac_addr='64:CC:22:3A:AC:BE', mud_url='a')

    def test_invalid_mac_format(self):
        self.initData()
        th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()

        self.assertPost(status.HTTP_400_BAD_REQUEST, "/thing/describe", mac_addr='b',
                        mud_url='https://lighting.example.com/lightbulb2000')

    def test_invalid_thing(self):
        self.initData()
        th = Thing(mac_addr="64:CC:22:3A:AC:BE", ipv4_addr="192.168.2.1", hostname="light.lan").save()

        self.assertPost(status.HTTP_404_NOT_FOUND, "/thing/describe", mac_addr='64:CC:22:3A:AC:BF',
                        mud_url='https://lighting.example.com/lightbulb2000')
