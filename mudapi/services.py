#!/usr/bin/env python3
import os
import pprint
import json
import subprocess
import urllib
import glob
import os
# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__file__)


import nmap
import random
from urllib.parse import urlsplit
from os import path
from git import Git, Repo
from muddy.maker import make_mud, make_acl_names, make_policy, make_acls, make_support_info
from muddy.models import Direction, IPVersion, Protocol, MatchType
from mudapi.models import MUD
from mudapi.models.queries import connections_of_mud
from neo4things.settings.base import BASE_DIR, GITLAB_TOKEN, GITLAB_URL, GITLAB_MASTER_PROJECT

import gitlab
import json


class UpdateMudChangeError(gitlab.exceptions.GitlabCreateError):
    pass


class UpdateMudChangeInstance:
    def __init__(self):
        self.conn = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
        self.conn.auth()
        self.master = self.conn.projects.get(GITLAB_MASTER_PROJECT)

    def update_change(self, name, mud_json):
        """MUD_json is the json file for the MUD file which is updated"""
        fork_name = 'MUD-' + name
        fork = "none"
        it = 0
        while 1:
            try:  # There is no atomic check-and-create in the gitlab API.
                # All we can really do here is rely on the server-side API to throw an exception if the project already exists
                fork = self.master.forks.create({'name': fork_name, 'path': fork_name})

            except gitlab.exceptions.GitlabCreateError as e:
                if e.response_code == 409:  # Connection successfull, but Project with that path already exists.
                    it = it + 1
                    fork_name = 'MUD-' + name + '-' + str(it)
                    continue
                else:  # Some other problem
                    raise UpdateMudChangeError(e)

            break

        fproject = self.conn.projects.get(fork.id)

        fproject.commits.create({
            'branch': 'master',
            'title': "[UPDATE MUD] " + name,
            'commit_message': "[UPDATE MUD] " + name,
            'actions': [{
                'action': 'update',
                'file_path': name,
                'content': json.dumps(mud_json, indent=4)
            }]
        })

        fproject.mergerequests.create(
            {'target_project_id': self.master.id,
             'source_branch': 'master',
             'target_branch': 'master',
             'title': '[UPDATE MUD] ' + name,
             'labels': ['generated', 'mud-update']})


def get_service_name(urlstring: str) -> str:
    return urllib.parse.urlsplit(urlstring, scheme='', allow_fragments=True).netloc


def clone(git_url, repo_dir):
    """This function clones the repo"""
    Repo.clone_from(git_url, repo_dir)


def pull(repo_dir):
    """This function pulls the repo"""
    g = Git(repo_dir)
    g.pull("origin", "master")


def sync(git_url, repo_dir):
    """This function makes sure the folder containing mud files is present
    example:  mudapi.services.sync('https://git.sr.ht/~s0/MUD-Files','mud-files')
    """
    print(repo_dir)
    if not path.isdir(repo_dir):
        clone(git_url, repo_dir)
    else:
        pull(repo_dir)
    import_muds()


def finder(ports, adress):
    """Find devices thich are in the network"""
    nm = nmap.PortScanner()
    nm.scan(hosts=adress, arguments="-sn")
    return nm.all_hosts()


def devicesup():
    """Search for devices which are up in the network"""
    nm = nmap.PortScanner()
    data = nm.scan(hosts="192.168.2.1/24", arguments="-sP")
    return data["nmap"]["scanstats"]["uphosts"]


def serialize_mud(mud):
    """
    Serialize json of a MUD file. Uses all Connection in the Databse
    """
    if not isinstance(mud, MUD):
        raise Exception

    support_info = make_support_info(mud.mud_version, mud.mud_url, mud.cache_validity, True, mud.systeminfo,
                                     mud.documentation)
    # Names to be used
    mud_name = f'mud-{random.randint(10000, 99999)}'
    acl = []
    policies = {}

    cons = connections_of_mud(mud.mud_url)
    policy_types = set()

    # Step 3 Create ACLS
    for con in cons:
        # IP Version
        if "4" in con.type:
            ver = IPVersion.IPV4
        else:
            ver = IPVersion.IPV6

        # Connection Type
        if "u" in con.type:
            con_type = Protocol.UDP
        if "t" in con.type:
            con_type = Protocol.TCP
        if "a" in con.type:
            con_type = Protocol.ANY

        # Parse Direction
        if "from-device" == con.direction_initiated:
            dir = Direction.FROM_DEVICE
        else:
            dir = Direction.TO_DEVICE

        #  Create acl-names
        if "from-device" == con.direction_initiated:
            policy_types.add((ver, Direction.FROM_DEVICE))
        else:
            policy_types.add((ver, Direction.TO_DEVICE))

        acl.append(make_acls([ver], str(con.acl_dns), con_type, MatchType.IS_MYMFG, dir, con.port, con.port,
                             make_acl_names(mud_name, ver, dir)))  # TODO MatchType ?

    for ver, dir in policy_types:
        acl_names = make_acl_names(mud_name, ver, dir)
        policies.update(make_policy(dir, acl_names))
    return make_mud(support_info, policies, acl)


def save_mud_gitlab(mud, name):
    """
    Saves the IR of a MUD to json and then creates a fork of the main file repo so that we can merge the changes into the main repo"""
    mud_json = serialize_mud(mud)
    gitlab = UpdateMudChangeInstance()
    gitlab.update_change(name=name, mud_json=mud_json)


def generate_mud(pmacAddress, pipAddress, pipv6Address, pdevicemac, pdeviceName, ppcaploc):
    """Create Mud file from pcap"""
    filepath = './mudgee/(%s).json' % pdeviceName
    data = {}
    data['defaultGatewayConfig'] = dict(macAddress=pmacAddress, ipAddress=pipAddress, ipv6Address=pipv6Address)
    data['deviceConfig'] = dict(device=pdevicemac, deviceName=pdeviceName)
    data['pcapLocation'] = ppcaploc
    with open(filepath, 'w') as outfile:
        json.dump(data, outfile)
    subprocess.call(['java', '-jar', './mudgee/mudgee-1.0.0-SNAPSHOT.jar', filepath])
    mudfilepath = './result/%s/%sMud.json' % (pdeviceName, pdeviceName)
    with open(mudfilepath, 'r') as file:
        return json.loads(file.read())


class MockRequest:
    def __init__(self, data):
        self.data = data


def import_muds():
    print("importing")
    from mudapi.views import UploadMudView
    directory = os.path.join(BASE_DIR, "files")
    for file in os.scandir(directory):
        if file.path.endswith(".json"):
            try:
                j = json.loads(open(file).read())
                a = MockRequest(data={"serial": json.dumps(j)})
                UploadMudView.post('/mud/upload', request=a)
                logger.info("Success Importing MUD file", os.path)
            except:
                logger.error("Error Importing MUD file", os.path)
