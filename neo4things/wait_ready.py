# Wait until database is ready
import os, time
from neomodel import db

from mudapi.models import MUD
from mudapi.services import sync
from neo4things.settings.base import NEOMODEL_NEO4J_BOLT_URL, GITLAB_URL, BASE_DIR, GITLAB_REPO
# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__file__)


if not NEOMODEL_NEO4J_BOLT_URL == "bolt://invalid":
    ready = False

    while not ready:
        try:
            db.set_connection(NEOMODEL_NEO4J_BOLT_URL)
            ready = True
        except Exception as e:
            logger.info(
                "Waiting for database ("
                + NEOMODEL_NEO4J_BOLT_URL
                + ") ("
                + str(e)
                + ")"
            )
            time.sleep(10)
            continue
# if len(MUD.nodes.all(lazy=True)) == 0:
#     logger.info("Initial Setup")
#     sync(GITLAB_REPO, os.path.join(BASE_DIR, "files"))
