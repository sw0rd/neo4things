from .base import *

DEBUG = False

ALLOWED_HOSTS = ["your_domain", "www.your_domain"]

SECURE_SSL_REDIRECT = True

SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True

SECURE_BROWSER_XSS_FILTER = True
