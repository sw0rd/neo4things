# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
from django.urls import include, path
from rest_framework import routers
from mudapi import views
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)


router = routers.DefaultRouter()
router.register(r"users", views.UserViewSet)
router.register(r"groups", views.GroupViewSet)
router.register(r"mud", views.MUDViewSet, "mud")
router.register(r"service", views.ServiceViewSet, "service")
router.register(r"manager", views.ManagerViewSet, "manager")
router.register(r"manufacturer", views.ManufacturerViewSet, "manufacturer")
router.register(r"thing", views.ThingViewSet, "thing")
router.register(r"os", views.OSViewSet, "os")

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("mud/dns", views.MUDByDNSView.as_view()),
    path("mud/upload", views.UploadMudView.as_view()),
    path("mud/domains", views.MUDByDNSView.as_view()),
    path("mud/schema", views.FindMUDBySchemaView.as_view()),
    path("mud/port", views.FindMUDByPortView.as_view()),
    path("mud/guessThing", views.GuessMUDByThingsConnections.as_view()),
    path("mud/thing", views.MUDOfAThing.as_view()),
    path("mud/byManufacturer", views.MudByManufacturerViewSet.as_view()),
    path("mud/json", views.JSONOfAMUD.as_view()),
    path("mud/connections", views.MUDConnections.as_view()),
    path("mud/generate", views.UploadPcapMudGeneratorViewSet.as_view()),
    path("mud/createFork", views.CreateFork.as_view()),
    path("thing/services", views.ServicesByThingView.as_view()),
    path("thing/allowedConnections", views.ThingAllowedConnections.as_view()),
    path("thing/connections", views.ThingConnections.as_view()),
    path("thing/mac", views.FindThingsByMacVendor.as_view()),
    path("thing/describe", views.ThingMUD.as_view()),
    path("manager/things", views.ThingsOfAManagerView.as_view()),
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path("pcap/", views.UploadPcapMudGeneratorViewSet.as_view()),

    # Optional UI:
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path(
        "api/schema/redoc/",
        SpectacularRedocView.as_view(url_name="schema"),
        name="redoc",
    ),
]
