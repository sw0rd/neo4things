from django import setup as setup_django
from django.core.management.base import BaseCommand
from django.core import management
from neomodel import db

from neo4things.settings.base import NEOMODEL_NEO4J_BOLT_URL, DATABASE_VERSION

INVALID_VERSION = 0

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__file__)


class Command(BaseCommand):
    help = "Check that the database is correct"

    def handle(self, *args, **options):
        setup_django()

        def set_version(version):
            db.cypher_query("""match (v:DBVERSION) delete v""")
            db.cypher_query(
                """create (v:DBVERSION {id: $version})""", {"version": version}
            )

        dbversion = 0
        try:
            dbversion = db.cypher_query(
                """ match (v:DBVERSION) with max(v.id) as max return max; """
            )[0][0][0]
            if not dbversion == DATABASE_VERSION:
                logger.warning("The database version does not match internal version; migrating")
                management.call_command("migrate")
                set_version(DATABASE_VERSION)
        except:
            logger.error("The databse seems to be empty or corrupt; installing labels")
            management.call_command("install_labels")
            management.call_command("migrate")
            set_version(DATABASE_VERSION)
