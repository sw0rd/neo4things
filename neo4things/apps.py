from django.apps import AppConfig


class WaitReady(AppConfig):
    name = "neo4things"
    verbose_name = "Wait until DB is ready"

    def ready(self):
        import neo4things.wait_ready
