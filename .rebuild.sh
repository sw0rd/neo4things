#!/usr/bin/env bash
set -euo pipefail

python manage.py clear_neo4j 
python manage.py install_labels 
printf "import example\nexit()" | python manage.py shell
