#!/bin/bash
echo "check database"
python3 manage.py validate_database
echo "run cronjobs"
python3 manage.py runcrons
echo "start programm"
python3 manage.py runserver 0.0.0.0:8000
