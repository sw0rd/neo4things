#Given a URL, return its geographic information. Information like Street, City, First-Level Subdivison and Country are usually gathered, but different Location may give different proprierties.
#YetAnotherLocationProgram_Locator.sh is an auxiliary script, and should the on the same folder this.
from sys import argv
import os
import subprocess
from geopy.geocoders import Nominatim

if len(argv) != 2:
    print("Too many arguments")

scriptPath = os.getcwd() + "/YetAnotherLocationProgram_locator.sh"
ip_location = subprocess.run([scriptPath, argv[1]], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

geolocator = Nominatim(user_agent="YetAnotherLocationProgram")
location = geolocator.reverse(ip_location.stdout)

print(location.raw,"\n")
print("County:",location.raw['address']['country'] )
